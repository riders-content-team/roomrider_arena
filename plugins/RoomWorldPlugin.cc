#include "RoomWorldPlugin.hh"

using namespace gazebo;
GZ_REGISTER_WORLD_PLUGIN(RoomWorldPlugin)

RoomWorldPlugin::RoomWorldPlugin()  : WorldPlugin()
{

}

RoomWorldPlugin::~RoomWorldPlugin()
{

}

void RoomWorldPlugin::Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
{
    this->world = _world;
    this->origin_x = -1.5;
    this->origin_y = -1.5;

	// Initialize ros, if it has not already bee initialized.
	if (!ros::isInitialized())
	{
		int argc = 0;
		char **argv = NULL;
		ros::init(argc, argv, "roomrider_world_plugin",
				  ros::init_options::NoSigintHandler);
	}

	// Create our ROS node. This acts in a similar manner to the Gazebo node
	this->rosNode.reset(new ros::NodeHandle());

	std::string topic = "collision";
	ros::SubscribeOptions so =
		ros::SubscribeOptions::create<std_msgs::String>(
			topic,
			10,
			boost::bind(&RoomWorldPlugin::collision_cb, this, _1),
			ros::VoidConstPtr(), NULL);

	//this->collision_subscriber = this->rosNode->subscribe(so);


    this->initial_load_stains();

    //this->worldUpdateConnection = event::Events::ConnectWorldUpdateBegin(
    //        std::bind(&RoomWorldPlugin::world_loop, this));

    this->worldResetConnection = event::Events::ConnectWorldReset(
            std::bind(&RoomWorldPlugin::world_reset, this));

}

void RoomWorldPlugin::collision_cb(const std_msgs::String::ConstPtr &msg)
{
    std::string collied_model = msg->data;

    for (int i=0; i < 16; i++)
    {
        if (this->stain_models[i] == collied_model)
        {
            this->stain_models[i] = "";

            ros::Time current_time = ros::Time::now();
            this->stain_models_with_time[i] = current_time.toSec();
        }
    }

}

void RoomWorldPlugin::world_loop()
{
    ros::Time current_time = ros::Time::now();
    double current_tine_in_secs = current_time.toSec();

    for (int i=0; i<4; i++)
    {
        for (int j=0; j<4; j++)
        {
            int index = (i * 4) + j;

            if(this->stain_models[index] == "" && (current_tine_in_secs - this->stain_models_with_time[index] > 20))
            {
                double pose_x = this-> origin_x + (double)j;;
                std::string pose_x_str = std::to_string(pose_x).substr(0,4);

                double pose_y = this-> origin_y + (double)i;
                std::string pose_y_str = std::to_string(pose_y).substr(0,4);

                int random_number = rand() % 6283;
                double pose_yaw = (double)random_number/1000;
                std::string pose_yaw_str = std::to_string(pose_yaw).substr(0,4);

                std::string stain_model_name;

                if ((i == 1 || i == 2) && (j == 1 || j == 2))
                {
                    stain_model_name = "stain_blue_" + pose_x_str + "_" + pose_y_str + "_" + pose_yaw_str;
                    this->spawn_stain_model("blue", pose_x_str, pose_y_str, pose_yaw_str);
                }
                else
                {
                    stain_model_name = "stain_pink_" + pose_x_str + "_" + pose_y_str + "_" + pose_yaw_str;
                    this->spawn_stain_model("pink", pose_x_str, pose_y_str, pose_yaw_str);
                }

                this->stain_models[index] = stain_model_name;
                this->stain_models_with_time[index] = current_tine_in_secs;
            }
        }
    }

}

void RoomWorldPlugin::world_reset()
{
    this->remove_all_models();

    this->stain_models.clear();

    this->initial_load_stains();
}

void RoomWorldPlugin::spawn_stain_model(std::string color ,std::string pose_x, std::string pose_y, std::string pose_yaw)
{
    std::string stain_model_name = "stain_" + color + "_" + pose_x + "_" + pose_y + "_" + pose_yaw;

    std::string stain_spawn_name = "<?xml version='1.0' ?> "
                                    "<sdf version='1.5'>"
                                    "<include>"
                                    "<pose>" + pose_x + " " + pose_y + " 0 0 0 " + pose_yaw + "</pose>"
                                    "<name> " + stain_model_name + "</name>"
                                    "<uri>model://stain_" + color + "</uri>"
                                    "</include>"
                                    "</sdf>";

    this->world->InsertModelString(stain_spawn_name);
}

void RoomWorldPlugin::initial_load_stains()
{
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j ++)
        {
            if(!(i==0 && j==0))
            {
                double pose_x = this-> origin_x + (double)j;
                std::string pose_x_str = std::to_string(pose_x).substr(0,4);

                double pose_y = this-> origin_y + (double)i;
                std::string pose_y_str = std::to_string(pose_y).substr(0,4);

                int random_number = rand() % 6283;
                double pose_yaw = (double)random_number/1000;
                std::string pose_yaw_str = std::to_string(pose_yaw).substr(0,4);

                std::string stain_model_name;

                if ((i == 1 || i == 2) && (j == 1 || j == 2))
                {
                    stain_model_name = "stain_blue_" + pose_x_str + "_" + pose_y_str + "_" + pose_yaw_str;
                    this->spawn_stain_model("blue", pose_x_str, pose_y_str, pose_yaw_str);
                }
                else
                {
                    stain_model_name = "stain_pink_" + pose_x_str + "_" + pose_y_str + "_" + pose_yaw_str;
                    this->spawn_stain_model("pink", pose_x_str, pose_y_str, pose_yaw_str);
                }

                this->stain_models.push_back(stain_model_name);

                ros::Time current_time = ros::Time::now();
                this->stain_models_with_time.push_back(current_time.toSec());
            }
            else
            {
                double pose_x = this-> origin_x + (double)j;
                std::string pose_x_str = std::to_string(pose_x).substr(0,4);

                double pose_y = this-> origin_y + (double)i;
                std::string pose_y_str = std::to_string(pose_y).substr(0,4);

                int random_number = rand() % 6283;
                double pose_yaw = (double)random_number/1000;
                std::string pose_yaw_str = std::to_string(pose_yaw).substr(0,4);

                std::string stain_model_name;
                stain_model_name = "stain_pink_" + pose_x_str + "_" + pose_y_str + "_" + pose_yaw_str;
                this->spawn_stain_model("pink", pose_x_str, pose_y_str, pose_yaw_str);

                this->stain_models.push_back(stain_model_name);

                ros::Time current_time = ros::Time::now();
                this->stain_models_with_time.push_back(current_time.toSec());
            }
        }
    }
}

void RoomWorldPlugin::remove_all_models ()
{
    for (int i=0; i<16; i++)
    {
        this->world->RemoveModel(this->stain_models[i]);
    }
}
