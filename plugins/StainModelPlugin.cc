#include "StainModelPlugin.hh"

using namespace gazebo;
GZ_REGISTER_MODEL_PLUGIN(StainModelPlugin)

StainModelPlugin::StainModelPlugin() : ModelPlugin()
{

}

StainModelPlugin::~StainModelPlugin()
{

}

void StainModelPlugin::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
{
	this->model = _model;
	this->stain_name = this->model->GetName();
	this->pose = this->model->RelativePose();
	this->isPicked = false;

	this->links = this->model->GetLinks();

	this->world = this->model->GetWorld();

	this->pickedupTime = this->world->SimTime().Double();

	// Initialize ros, if it has not already bee initialized.
	if (!ros::isInitialized())
	{
		int argc = 0;
		char **argv = NULL;
		ros::init(argc, argv, this->stain_name,
				  ros::init_options::NoSigintHandler);
	}

	// Create our ROS node. This acts in a similar manner to the Gazebo node
	this->rosNode.reset(new ros::NodeHandle());

	this->stain_collision_pub = this->rosNode->advertise<std_msgs::String>("collision", 1000);

	std::string topic = "robot_pose";
	ros::SubscribeOptions so =
		ros::SubscribeOptions::create<std_msgs::String>(
			topic,
			10,
			boost::bind(&StainModelPlugin::onPoseReceive, this, _1),
			ros::VoidConstPtr(), NULL);

	this->robot_pose_subscriber = this->rosNode->subscribe(so);

	this->worldConnection = event::Events::ConnectWorldUpdateBegin(
				std::bind(&StainModelPlugin::Animate, this));

}


void StainModelPlugin::onPoseReceive(const std_msgs::StringConstPtr &msg)
{
	std::string data = msg->data.c_str();
	std::string delimiter = ":";

	std::vector<std::string> params;

	size_t pos = 0;
	std::string token;
	while ((pos = data.find(delimiter)) != std::string::npos)
	{
		token = data.substr(0, pos);
		params.push_back(token);
		data.erase(0, pos + delimiter.length());
	}
	params.push_back(data);
	double robot_x = std::stof(params[1]);
	double robot_y = std::stof(params[2]);

	if ((abs(robot_x - this->pose.Pos().X()) < 0.1) &&
		(abs(robot_y - this->pose.Pos().Y()) < 0.1))
	{
		if(not this->isPicked)
		{
				this->onPickup();
		}
	}
}

void StainModelPlugin::onPickup()
{
	std_msgs::String msg;
	msg.data = this->stain_name;
	stain_collision_pub.publish(msg);

	ignition::math::Pose3d new_pose = ignition::math::Pose3d(
		this->pose.Pos().X(),
		this->pose.Pos().Y(),
		-0.12,
		0,
		0,
		this->pose.Rot().Yaw()
	);

	for (std::vector<physics::LinkPtr>::iterator li = this->links.begin();
	li != this->links.end(); ++li)
	{
		this->model->SetLinkWorldPose(new_pose, (*li));
	}
	this->isPicked = true;
	this->pickedupTime = this->world->SimTime().Double();
}

void StainModelPlugin::Animate()
{
	if(this->isPicked)
	{
		double time = this->world->SimTime().Double();
		double ellapsed_time = time - pickedupTime;
		std::cout << ellapsed_time << '\n';
		if(ellapsed_time > 20) {
			ignition::math::Pose3d new_pose = ignition::math::Pose3d(
				this->pose.Pos().X(),
				this->pose.Pos().Y(),
				0,
				0,
				0,
				this->pose.Rot().Yaw()
			);

			for (std::vector<physics::LinkPtr>::iterator li = this->links.begin();
			li != this->links.end(); ++li)
			{
				this->model->SetLinkWorldPose(new_pose, (*li));
			}
			this->isPicked = false;
		}
		else {
			double pos_z = (ellapsed_time ) * (0.12 / 20) - 0.12;
			ignition::math::Pose3d new_pose = ignition::math::Pose3d(
				this->pose.Pos().X(),
				this->pose.Pos().Y(),
				pos_z,
				0,
				0,
				this->pose.Rot().Yaw()
				);
				for (std::vector<physics::LinkPtr>::iterator li = this->links.begin();
				li != this->links.end(); ++li)
				{
					this->model->SetLinkWorldPose(new_pose, (*li));
				}
		}
	}
  if (ros::ok())
  {
    ros::spinOnce();
  }
}
