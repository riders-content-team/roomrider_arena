#ifndef GAZEBO_PLUGINS_ROOM_WORLD_PLUGIN
#define GAZEBO_PLUGINS_ROOM_WORLD_PLUGIN

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"

#include "gazebo/physics/physics.hh"
#include "gazebo/gazebo.hh"
#include "gazebo/common/common.hh"

namespace gazebo
{
    class GAZEBO_VISIBLE RoomWorldPlugin : public WorldPlugin
    {
        public: RoomWorldPlugin();
        public: ~RoomWorldPlugin();
    
        public: virtual void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf);

        private: void remove_all_models();
        private: void initial_load_stains();
        private: void spawn_stain_model(std::string color, std::string pose_x, std::string pose_y, std::string pose_yaw);

        private: double origin_x, origin_y;
        private: std::vector<std::string> stain_models;
        private: std::vector<double> stain_models_with_time;

        private: event::ConnectionPtr worldUpdateConnection;
        private: event::ConnectionPtr worldResetConnection;
        private: void world_loop();
        private: void world_reset();
        
        private: void collision_cb(const std_msgs::String::ConstPtr &msg);

		private: std::unique_ptr<ros::NodeHandle> rosNode;
		private: ros::Subscriber collision_subscriber;

        private: physics::WorldPtr world;
    };
}

#endif
